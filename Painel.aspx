﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Painel.aspx.cs" Inherits="PainelBootcampIBID.Painel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="StyleSheet1.css" rel="stylesheet" type="text/css" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            width: 144px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table style="width: 310px;">
            <tr style="margin-left: 15px">
                <td class="panel_title"><h1>Painel Bootcamp IBID</h1></td>
            </tr>
            <tr style="margin-left: 15px">
                <td>
                    <asp:Panel ID="Panel1" runat="server" CssClass="painel1">
                        <asp:Panel ID="Panel2" runat="server" CssClass="painel">
                            <table style="width: 100%;">
                                <tr>
                                    <td class="panel_title" colspan="2">Informações pessoais</td>
                                </tr>
                                <tr>
                                    <td class="indicador">Nome:</td>
                                    <td class="auto-style1">
                                        <asp:TextBox ID="TextBoxNome" runat="server"></asp:TextBox>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="indicador">Sobrenome:</td>
                                    <td class="auto-style1">
                                        <asp:TextBox ID="TextBoxSobrenome" runat="server"></asp:TextBox>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="indicador">Gênero:</td>
                                    <td class="auto-style1">
                                        <asp:TextBox ID="TextBoxGenero" runat="server"></asp:TextBox>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="indicador">Celular:</td>
                                    <td class="auto-style1">
                                        <asp:TextBox MaxLength="15"
                                            runat="server" ID="TextBoxCelular"
                                            placeholder="(xx) xxxxx-xxxx"
                                            onkeydown="javascript:backspacerDOWN(this,event);"
                                            onkeyup="javascript:backspacerUP(this,event);"
                                            ></asp:TextBox>

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="enviar_bt">
                                        <asp:Button ID="ButtonProximo1" runat="server" OnClick="Proximo" Text="Próximo" />

                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="Panel3" runat="server" CssClass="painel" Visible="False">
                            <table style="width: 100%;">
                                <tr>
                                    <td class="panel_title" colspan="2">Detalhes do Endereço</td>
                                </tr>
                                <tr>
                                    <td class="indicador">Endereço:</td>
                                    <td class="auto-style1">
                                        <asp:TextBox ID="TextBoxEndereco" runat="server"></asp:TextBox>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="indicador">Cidade:</td>
                                    <td class="auto-style1">
                                        <asp:TextBox ID="TextBoxCidade" runat="server"></asp:TextBox>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="indicador">CEP:</td>
                                    <td class="auto-style1">
                                        <asp:TextBox ID="TextBoxCEP" placeholder="xxxxx-xxx" runat="server" onkeyup="formatCEP(this);" />

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="enviar_bt">
                                        <asp:Button ID="ButtonVoltar1" runat="server" OnClick="Voltar" Text="Voltar" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="enviar_bt">
                                        <asp:Button ID="ButtonProximo2" runat="server" OnClick="Proximo" Text="Próximo" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="Panel4" runat="server" CssClass="painel" Visible="False">
                            <table style="width: 100%;">
                                <tr>
                                    <td class="panel_title" colspan="2">Área de Login</td>
                                </tr>
                                <tr>
                                    <td class="indicador">Usuário:</td>
                                    <td class="auto-style1">
                                        <asp:TextBox ID="TextBoxUsuario" runat="server"></asp:TextBox>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="indicador">Senha:</td>
                                    <td class="auto-style1">
                                        <asp:TextBox ID="TextBoxSenha" TextMode="Password" runat="server"></asp:TextBox>

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="enviar_bt">
                                        <asp:Button ID="ButtonVoltar2" runat="server" OnClick="Voltar" Text="Voltar" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="enviar_bt">
                                        <asp:Button ID="ButtonEnviar" runat="server" OnClick="Enviar" Text="Enviar" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
<script type="text/javascript">
    // Formatar celular
    var zChar = new Array(' ', '(', ')', '-');
    var maxphonelength = 14; // Alterado para acomodar o formato brasileiro
    var phonevalue1;
    var phonevalue2;
    var cursorposition;

    function ParseForNumber1(object) {
        phonevalue1 = ParseChar(object.value, zChar);
    }

    function ParseForNumber2(object) {
        phonevalue2 = ParseChar(object.value, zChar);
    }

    function backspacerUP(object, e) {
        if (e) {
            e = e;
        } else {
            e = window.event;
        }
        if (e.which) {
            var keycode = e.which;
        } else {
            var keycode = e.keyCode;
        }

        ParseForNumber1(object);

        if (keycode >= 48) {
            ValidatePhone(object);
        }
    }

    function backspacerDOWN(object, e) {
        if (e) {
            e = e;
        } else {
            e = window.event;
        }
        if (e.which) {
            var keycode = e.which;
        } else {
            var keycode = e.keyCode;
        }
        ParseForNumber2(object);
    }

    function GetCursorPosition() {
        var t1 = phonevalue1;
        var t2 = phonevalue2;
        var bool = false;
        for (i = 0; i < t1.length; i++) {
            if (t1.substring(i, 1) != t2.substring(i, 1)) {
                if (!bool) {
                    cursorposition = i;
                    bool = true;
                }
            }
        }
    }

    function ValidatePhone(object) {
        var p = phonevalue1;
        p = p.replace(/[^\d]*/gi, "");

        if (p.length < 2) {
            object.value = p;
        } else if (p.length == 2) {
            pp = p;
            d4 = p.indexOf('(');
            if (d4 == -1) {
                pp = "(" + pp;
            }
            object.value = pp;
        } else if (p.length > 2 && p.length < 7) {
            p = "(" + p;
            l30 = p.length;
            p30 = p.substring(0, 3);
            p30 = p30 + ")";
            p31 = p.substring(3, l30);
            pp = p30 + p31;
            object.value = pp;
        } else if (p.length >= 7) {
            p = "(" + p;
            l30 = p.length;
            p30 = p.substring(0, 3);
            p30 = p30 + ")";
            p31 = p.substring(3, l30);
            pp = p30 + p31;
            l40 = pp.length;
            p40 = pp.substring(0, 9);
            p40 = p40 + "-";
            p41 = pp.substring(9, l40);
            ppp = p40 + p41;
            object.value = ppp.substring(0, maxphonelength);
        }

        GetCursorPosition();

        if (cursorposition >= 0) {
            if (cursorposition == 0) {
                cursorposition = 2;
            } else if (cursorposition <= 2) {
                cursorposition = cursorposition + 1;
            } else if (cursorposition <= 6) {
                cursorposition = cursorposition + 2;
            } else if (cursorposition == 7) {
                cursorposition = cursorposition + 4;
                e1 = object.value.indexOf(')');
                e2 = object.value.indexOf('-');
                if (e1 > -1 && e2 > -1) {
                    if (e2 - e1 == 4) {
                        cursorposition = cursorposition - 1;
                    }
                }
            } else if (cursorposition < 11) {
                cursorposition = cursorposition + 3;
            } else if (cursorposition == 11) {
                cursorposition = cursorposition + 1;
            } else if (cursorposition >= 12) {
                cursorposition = cursorposition;
            }

            var txtRange = object.createTextRange();
            txtRange.moveStart("character", cursorposition);
            txtRange.moveEnd("character", cursorposition - object.value.length);
            txtRange.select();
        }
    }

    function ParseChar(sStr, sChar) {
        if (sChar.length == null) {
            zChar = new Array(sChar);
        } else zChar = sChar;

        for (i = 0; i < zChar.length; i++) {
            sNewStr = "";
            var iStart = 0;
            var iEnd = sStr.indexOf(sChar[i]);

            while (iEnd != -1) {
                sNewStr += sStr.substring(iStart, iEnd);
                iStart = iEnd + 1;
                iEnd = sStr.indexOf(sChar[i], iStart);
            }
            sNewStr += sStr.substring(sStr.lastIndexOf(sChar[i]) + 1, sStr.length);

            sStr = sNewStr;
        }

        return sNewStr;
    }

    function formatCEP(textBox) {
        var cep = textBox.value.replace(/\D/g, '');

        if (cep.length == 8) {
            textBox.value = cep.substring(0, 5) + '-' + cep.substring(5, 8);
        }
    }
</script>