﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PainelBootcampIBID
{
    public partial class Painel : System.Web.UI.Page
    {
        string formato_celular = "(##) #####-####";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Proximo(object sender, EventArgs e)
        {
            /*if (nome == "" || sobrenome == "" || genero == "" || celular == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Algum campo está vazio!')", true);
                return;
            }*/

            switch (((Button) sender).ID) {
                case "ButtonProximo1":
                    Panel2.Visible = false;
                    Panel3.Visible = true;
                    break;
                case "ButtonProximo2":
                    Panel3.Visible = false;
                    Panel4.Visible = true;
                    break;
            }        
        }

        protected void Voltar(object sender, EventArgs e)
        {
            switch (((Button)sender).ID)
            {
                case "ButtonVoltar1":
                    Panel2.Visible = true;
                    Panel3.Visible = false;
                    break;
                case "ButtonVoltar2":
                    Panel3.Visible = true;
                    Panel4.Visible = false;
                    break;
            }
        }

        public void LimparTextBoxes(Control parent)
        {
            foreach (Control control in parent.Controls)
            {
                if (control is TextBox)
                {
                    TextBox textBox = (TextBox)control;
                    textBox.Text = string.Empty;
                }
                else if (control.HasControls())
                {
                    LimparTextBoxes(control);
                }
            }
        }

        protected void Enviar(object sender, EventArgs e)
        {
            var campos = new Dictionary<TextBox, string>();
            campos.Add(TextBoxNome, "nome");
            campos.Add(TextBoxSobrenome, "Sobrenome");
            campos.Add(TextBoxGenero, "Gênero");
            campos.Add(TextBoxCelular, "Celular");
            campos.Add(TextBoxEndereco, "Endereço");
            campos.Add(TextBoxCidade, "Cidade");
            campos.Add(TextBoxCEP, "CEP");
            campos.Add(TextBoxUsuario, "Usuário");
            campos.Add(TextBoxSenha, "Senha");

            foreach (var par  in campos)
            {
                if (par.Key.Text == "")
                {
                    Label1.Text = "ERRO! Campo " + par.Value + " está vazio.";
                    Label1.Visible = true;
                    return;
                }
            }

            LimparTextBoxes(this);
            Label1.Text = "AVISO! Seus dados foram enviados com sucesso.";
            Label1.Visible = true;
        }
    }
}